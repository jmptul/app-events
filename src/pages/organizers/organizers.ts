import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase/app';
import { OrganizerDetailsPage } from '../organizer-details/organizer-details';

@IonicPage()
@Component({
  selector: 'page-organizers',
  changeDetection: ChangeDetectionStrategy.Default,
  templateUrl: 'organizers.html',
})
export class OrganizersPage {

  organizers: any = [];
  organizersList: any = [];
  feedOrganizers: any = [];
  loading: boolean = true;
  number_followers: number;
  searchQuery: any = "";

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private cdRef:ChangeDetectorRef) {
  }

  ionViewWillEnter(){
    this.organizers = [];
    this.feedOrganizers = [];
    this.getOrganizers();
    this.loading = false;
  }

  initializeItems() {
    this.organizers = this.organizersList;
  }

  async getOrganizers() {
    let organizersOnLoad = [];
    var ref = firebase.database().ref('/organizers/');
    ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        organizersOnLoad.push(child.val());
      })
    }).then(() => {
      this.organizersList = organizersOnLoad;
      this.organizers = organizersOnLoad;  
    })
    await this.getOrganizerOfCurrentUser();
    if (!this.cdRef['destroyed']) {
      this.cdRef.detectChanges();
    }
  }

  followOrganizer(organizer){
    var uid_organizer = organizer.UID;
    var uid_user = firebase.auth().currentUser.uid;
    var ref = firebase.database().ref('/users/'+uid_user+'/feed');
    ref.child(uid_organizer).set(uid_organizer)
    .then(() => {
      this.feedOrganizers.push(uid_organizer);
      this.getCounterOrganizer(organizer.UID);
      this.getCounterUser();
    })
  }

  getCounterUser() {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/followQuantity')
    ref.once('value', (snapshot) => {
      var followQuantityOld = snapshot.val();
      var followQuantityNew = followQuantityOld + 1;
      this.updateCounterUser(followQuantityNew);
    })
  }

  getCounterOrganizer(organizerUID) { 
    var ref = firebase.database().ref('/organizers/'+organizerUID+'/number_followers');
    ref.once('value', (snapshot) => {
      this.number_followers = snapshot.val()
      this.number_followers++
      this.updateCounterOrganizer(organizerUID);
    })
  }

  updateCounterOrganizer(organizerUID) {
    var db = firebase.database();
    var ref = db.ref().child('/organizers/'+organizerUID);
    ref.update({
      number_followers: this.number_followers
    })
  }

  updateCounterUser(newValue) {
    var db = firebase.database();
    var ref = db.ref().child('/users/'+firebase.auth().currentUser.uid);
    ref.update({
      followQuantity: newValue,
    })
  }

  async getOrganizerOfCurrentUser() {
    var uid_user = firebase.auth().currentUser.uid;
    var ref = firebase.database().ref('/users/'+uid_user+'/feed')
    await ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        this.feedOrganizers.push(child.key)
      })
    })
  }

  follow(organizer):boolean{
    for(var i = 0; i < this.feedOrganizers.length; i++){
      if (this.feedOrganizers[i] == organizer.UID) {
        return true;
      }
    }
    return false;
  }

  unfollow(organizer) {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/feed').child(organizer.UID);
    ref.remove()
    .then(() => {
      this.updateCounterOrganizerDown(organizer.UID);
      this.updateCounterUserDown();
    })
    this.feedOrganizers = this.arrayRemove(this.feedOrganizers, organizer.UID)
  }

  updateCounterUserDown() {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/followQuantity');
    ref.once('value', (snapshot) => {
      var followQuantityOld = snapshot.val()
      var followQuantityNew = followQuantityOld - 1;
      this.updateDownUser(followQuantityNew);
    })
  }

  updateCounterOrganizerDown(organizerUID) {
    var ref = firebase.database().ref('/organizers/'+organizerUID+'/number_followers');
    ref.once('value', (snapshot) => {
      this.number_followers = snapshot.val()
      this.number_followers--;
      this.updateDownOrganizer(organizerUID);
    })
  }

  updateDownUser(newValue) {
    var ref = firebase.database().ref().child('/users/'+firebase.auth().currentUser.uid);
    ref.update({
      followQuantity: newValue,
    })
  }
  updateDownOrganizer(organizerUID) {
    var db = firebase.database();
    var ref = db.ref().child('/organizers/'+organizerUID);
    ref.update({
      number_followers: this.number_followers
    })
  }

  arrayRemove(arr, value) {
    return arr.filter(function(ele){
        return ele != value;
    });
 }

 openOrganizer(organizer){
    this.navCtrl.push(OrganizerDetailsPage, organizer);
 }

 async search() {
  this.initializeItems();

  if(this.searchQuery.length < 1){
    return;
  }

  this.organizers = this.organizers.filter((organizer) => organizer.name.toLowerCase().includes(this.searchQuery.toLowerCase()));
}
}
