import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrganizersPage } from './organizers';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    OrganizersPage,
  ],
  imports: [
    IonicPageModule.forChild(OrganizersPage),
    TranslateModule,
  ],
})
export class OrganizersPageModule {}
