import { ProfilePage } from './account';
import { ProfileComponentModule } from '../../components/profile/profile.component.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';


@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    ProfileComponentModule,
    IonicPageModule.forChild(ProfilePage),
  ],
})
export class ProfilePageModule {}
