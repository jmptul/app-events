import { TicketsPage } from '../tickets/tickets';
import { FavouritesPage } from '../favourites/favourites';
import { ProfileDetailsPage } from '../profile-details/profile-details';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'account.html',
})
export class ProfilePage {
  tabProfileDetails: any;
  tabFavourites: any;
  tabTickets: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tabProfileDetails = ProfileDetailsPage;
    this.tabFavourites = FavouritesPage;
    this.tabTickets = TicketsPage;
  }
}
