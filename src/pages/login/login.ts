import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../app/models/user.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Keyboard } from 'ionic-angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loading: boolean;
  user = {} as User;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public Keyboard: Keyboard,
    private afAuth: AngularFireAuth,
    private nativePageTransitions: NativePageTransitions) {
      
  }
  goToRegisterPage() {
    let options: NativeTransitionOptions = {
      direction: 'left',
      duration: 400,
      slowdownfactor: -1,
      iosdelay: 50
     };
 
    this.nativePageTransitions.slide(options);
    this.navCtrl.push('RegisterPage');
  }


  async login() {
    this.loading = true;
    await this.afAuth.auth.signInWithEmailAndPassword(this.user.email, this.user.password)
    .then(() => {
      this.navCtrl.setRoot('MainPage');
    }).catch ((e) => {
      this.loading = false;
    })
  }
}
