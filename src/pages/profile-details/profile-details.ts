import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-profile-details',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'profile-details.html',
})
export class ProfileDetailsPage {

  name: string;
  lastname: string;
  phone: 0;
  direction: string;
  gender: string;
  profileData: any;
  key: string;
  

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private cdRef:ChangeDetectorRef,
    private toastCtrl: ToastController) {
    var ref = firebase.database().ref('/users/');
    ref.on('value', (snapshot) => {
      snapshot.forEach((child) => {
        if(child.val().UID == firebase.auth().currentUser.uid){
          this.profileData = [{
            name: child.val().name,
            lastname: child.val().lastname,
            phone: child.val().phone,
            direction: child.val().direction,
            followQuantity: child.val().followQuantity,
            points: child.val().points,
            sex: child.val().sex,
          }];
          if (!this.cdRef['destroyed']) {
            this.cdRef.detectChanges();
          }
        }
      });
    })
  }
  
  updateUserInformation(){
    firebase.database().ref('/users/'+firebase.auth().currentUser.uid)
    .update({
        name: this.name,
        lastname: this.lastname,
        phone: this.phone,
        direction: this.direction,
        location: {
          latitude: 0,
          longitude: 0,
        },
        sex: this.gender,
      })
      .then(() => {
        const toast = this.toastCtrl.create({
          message: 'Perfil actualizado correctamente',
          duration: 3000
        });
  
        toast.present();
      })
      .catch(() => {
        const toast = this.toastCtrl.create({
          message: 'Error',
          duration: 3000
        });
  
        toast.present();
      })
  }

  selectGenderMale() {
    this.gender = "Male"
  }

  selectGenderFemale() {
    this.gender = "Female"
  }
}
