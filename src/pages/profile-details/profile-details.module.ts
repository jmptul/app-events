import { ProfileComponentModule } from './../../components/profile/profile.component.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { ProfileDetailsPage } from './profile-details';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [
    ProfileDetailsPage,
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileComponentModule,
    IonicPageModule.forChild(ProfileDetailsPage),
    TranslateModule,
  ],
})
export class ProfileDetailsPageModule {}
