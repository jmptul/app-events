import { User } from './../../app/models/user.model';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase/app';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Keyboard } from 'ionic-angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  user = {} as User;
  loading: boolean;

  constructor(public navCtrl: NavController, 
    private afAuth: AngularFireAuth,
    public navParams: NavParams, 
    public Keyboard:Keyboard,
    private nativePageTransitions: NativePageTransitions) {
  }

  goToLoginPage() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 400,
      slowdownfactor: -1,
      iosdelay: 50
     };
 
    this.nativePageTransitions.slide(options);
    this.navCtrl.push('LoginPage');
  }

  async register(user) {
    this.loading = true;
    await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
      .then((result) => {
        var data = {
          tickets: 'none',
          favourites: 'none',
          name: '',
          lastname: '',
          location: {latitude: 0, longitude: 0},
          direction: '',
          sex: '',
          followQuantity: 0,
          following: 'none',
          points: 0,
          UID: result.user.uid,
          email: result.user.email,
          feed: 'none',
          phone: 0,
          isFirst: true,
          image: 'https://firebasestorage.googleapis.com/v0/b/socialzit.appspot.com/o/default%2Fprofile_image_default.png?alt=media&token=0d36d555-c2a4-46f3-ac7a-0a5cb5191f31',
        }
        this.navCtrl.setRoot('MainPage');
        firebase.database().ref('/users/').child(result.user.uid).set(data);
      })
      .catch(() => {
        this.loading = false;
      })
  }
}
