import { PaymentDataPage } from './../payment-data/payment-data';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ToastController, ModalController } from 'ionic-angular';
import * as  moment  from 'moment';
import * as  firebase  from 'firebase';
import { User } from '../../app/models/user.model';

@IonicPage()
@Component({
  selector: 'page-event-details',
  templateUrl: 'event-details.html',
})
export class EventDetailsPage{

  user = {} as User;
  event: any;
  dateEvent: any;
  tickets: any = [];
  slideOpts = {
    effect: 'flip'
  };
  timestamp: number;
  ticketSelected: boolean = false;
  select_option: any;
  ticket: any;
  image_organizer: any;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public platform: Platform,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController) {
    this.event = navParams.data;
  }

  ionViewWillEnter(){
    this.dateEvent = this.getDate();
    this.getImageOrganizer();
    this.getTickets();
    var timeEvent = this.event.timestamp;
    var timeNow = moment().unix();
    var difference = (timeNow - timeEvent);
    this.timestamp = Math.floor(difference/60/60/24);
  }

  goBack() {
    this.navCtrl.pop();
  }  

  getImageOrganizer(){
    var ref = firebase.database().ref('/organizers/')
    ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        if(child.val().name == this.event.name_organizer) this.image_organizer = child.val().image_profile
      })
    })
  }

  getDate() { return this.event.date.substring(8, 10) + "/" + this.event.date.substring(5, 7) + "/" + this.event.date.substring(0, 4); }

  getTickets() {this.tickets = this.event.tickets;}

  openMap() {window.open('geo:0,0?q='+this.event.latitude+','+this.event.longitude+'('+this.event.place+')','_system');}

  selectTicket(ticket) {
    this.user.buy = true;
    this.ticket = ticket;
  }

  buyTicket(nameOrganizer) {
    if(this.user.buy && this.ticket.quantityTickets > 0){
      const modal = this.modalCtrl.create(PaymentDataPage, {ticket: this.ticket, nameOrganizer: nameOrganizer, event: this.event});
      modal.present();
    }else if(this.ticket.quantityTickets < 1){
        this.toastCtrl.create({
          message: 'No quedan tickets',
          duration: 3000
        }).present();
    }else{
      this.toastCtrl.create({
        message: 'No ha seleccionado ningun ticket',
        duration: 3000
      }).present();
    }
  }
}
