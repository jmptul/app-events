import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventDetailsPage } from './event-details';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    EventDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(EventDetailsPage),
    TranslateModule,
  ],
  providers: [
  ]
})
export class EventDetailsPageModule {}
