import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import JsBarcode from 'jsbarcode';

@IonicPage()
@Component({
  selector: 'page-ticket-details',
  templateUrl: 'ticket-details.html',
})
export class TicketDetailsPage {

  ticket: any = [];
  public picture;
  @ViewChild('barcode') barcode: ElementRef;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams) {
      this.ticket = navParams.get('ticket');
      
  }

  ionViewDidLoad(){
    JsBarcode(this.barcode.nativeElement, this.ticket.ticketTitle);
  }

  openMap() {
    window.open('geo:0,0?q='+this.ticket.ticketPlaceLat+','+this.ticket.ticketPlaceLon+'('+this.ticket.ticketPlace+')','_system');
  }

  closeModal(){
    this.navCtrl.pop();
  }


}
