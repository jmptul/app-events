import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TicketDetailsPage } from './ticket-details';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    TicketDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(TicketDetailsPage),
    TranslateModule,
  ],
})
export class TicketDetailsPageModule {}
