import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import firebase from 'firebase/app';
import { PromotionDetailsPage } from '../promotion-details/promotion-details';

@IonicPage()
@Component({
  selector: 'page-points',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'points.html',
})
export class PointsPage {
  promotions: any = [];
  points: number = 0;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private cdRef: ChangeDetectorRef,
    public toastCtrl: ToastController) {
  }

  ionViewWillEnter(){
    this.promotions = [];
    this.getPoints();
    this.getPromotions();
  }

  getPoints() {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid)
    ref.once('value', (snapshot) => {
      this.points = snapshot.val().points
    })
  }

  getPromotions() {
    var ref = firebase.database().ref('/promotions')
    ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        child.forEach((otherChild) => {
          this.promotions.push(otherChild.val())
          console.log("HHHH:", this.promotions)
        })
      })
    })
    .then(() => {
      if (!this.cdRef['destroyed']) {
        this.cdRef.detectChanges();
      }
    })
  }

  openDetails(promotion) {
    if(this.points >= promotion.pointsPromotion){
      this.navCtrl.push(PromotionDetailsPage, promotion);
    }else{
      const toast = this.toastCtrl.create({
        message: "No tiene puntos suficientes para acceder a esta promocion",
        duration: 3000
      })

      toast.present();
    }
  }

}
