import { ProfileComponentModule } from './../../components/profile/profile.component.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TicketsPage } from './tickets';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    TicketsPage,
  ],
  imports: [
    ProfileComponentModule,
    IonicPageModule.forChild(TicketsPage),
    TranslateModule,
  ],
})
export class TicketsPageModule {}
