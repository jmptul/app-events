import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import firebase from 'firebase/app';
import { TicketDetailsPage } from '../ticket-details/ticket-details';

@IonicPage()
@Component({
  selector: 'page-tickets',
  templateUrl: 'tickets.html',
})
export class TicketsPage {

  tickets: any = []

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public modalCtrl: ModalController) {
  }

  async ionViewWillEnter(){
    this.tickets = [];
    await this.getTickets();
  }

  async getTickets() {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/tickets')
    await ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        this.tickets.push(child.val())
      })
    })
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.ionViewWillEnter()
    .then(() => {
      refresher.complete();
    });
  }

  goToTicketDetails(ticket){
    const modal = this.modalCtrl.create(TicketDetailsPage, {ticket: ticket});
    modal.present();
  }

  getRefToDelete(ticket) {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/tickets')
    ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        if(child.val().ticketTitle == ticket.ticketTitle && 
          child.val().ticketPrice == ticket.ticketPrice &&
          child.val().ticketEventName == ticket.ticketEventName){
          this.deleteTicket(child.key, ticket);
        }
      })
    })
  }

  deleteTicket(keyTicket, ticket){
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/tickets/'+keyTicket)
    ref.remove()
    .then(() => {
      var index = this.tickets.indexOf(ticket);
      this.tickets.splice(index, 1);
    })
  }

}
