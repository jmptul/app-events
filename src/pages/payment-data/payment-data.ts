import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Toast, ToastController } from 'ionic-angular';
import firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-payment-data',
  templateUrl: 'payment-data.html',
})
export class PaymentDataPage {

  nameUser: string;
  lastNameUser: string;
  directionUser: string;
  ticket: any;
  nameOrganizer: any;
  keyOrganizerGlobal: any;
  event: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl:ToastController) {
    this.ticket = navParams.get('ticket');
    this.nameOrganizer = navParams.get('nameOrganizer');
    this.event = navParams.get('event');
  }

  ionViewWillEnter(){
    this.getDataUser();
  }

  async getDataUser(){
    var ref = firebase.database().ref('/users/');
    await ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        if(child.key == firebase.auth().currentUser.uid){
          this.nameUser = child.val().name;
          this.lastNameUser = child.val().lastname;
          this.directionUser = child.val().direction;
        }
      })
    })
  }

  closeModal(){
    this.navCtrl.pop();
  }

  buyTicket() {
    var ref = firebase.database().ref('/organizers/');
    ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        if(child.val().name == this.nameOrganizer){
          this.getEventKey(child.key);
        }
      })
    }).then(() => {
      this.toastCtrl.create({
        message: 'Compra realizada',
        duration: 3000,
      }).present().then(() => {
        this.closeModal();
      });
    })
  }

  getEventKey(keyOrganizer){
    var ref = firebase.database().ref('/organizers/'+keyOrganizer+'/events')
    ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        if(child.val().title == this.event.title){
          this.getNumberTickets(keyOrganizer, child.key);
        }
      })
    })
  }

  getNumberTickets(keyOrganizer, keyEvent){
    var ref = firebase.database().ref('/organizers/'+keyOrganizer+'/events/'+keyEvent+'/tickets');
    ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        if(child.val().titleTicket == this.ticket.titleTicket && child.val().ticketPrice == this.ticket.ticketPrice){
          if(child.val().quantityTickets > 0){
            this.updateCounterTicket(keyOrganizer, keyEvent, child.key, child.val().quantityTickets);
          }
        }
      })
    })
  }

  updateCounterTicket(keyOrganizer, keyEvent, keyTicket, quantityTickets) {
    this.keyOrganizerGlobal = keyOrganizer;
    var quantity = quantityTickets - 1;
    console.log(quantity)
    var ref = firebase.database().ref('/organizers/'+keyOrganizer+'/events/'+keyEvent+'/tickets/'+keyTicket);
    ref.update({
      quantityTickets: quantity,
    })
    .then(() => {
      this.pushTicketOnUserProfile();
      
    })
  }

  pushTicketOnUserProfile() {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/tickets')
    ref.push({
      ticketTitle: this.ticket.titleTicket,
      ticketPrice: this.ticket.ticketPrice,
      ticketEventName: this.event.title,
      ticketOrganizer: this.nameOrganizer,
      ticketEventStart: this.event.timeStart,
      ticketEventEnd: this.event.timeEnd,
      ticketPlace: this.event.place,
      ticketPlaceLat: this.event.latitude,
      ticketPlaceLon: this.event.longitude,
      ticketEventDate: this.event.date,
    })
    .then(() => {
      this.getPointsUser(this.ticket.ticketPrice);
    })
  }

  getPointsUser(price) {
    var points = 0;
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/points')
    ref.once('value', (snapshot) => {
      points = snapshot.val()
    })
    .then(() => {
      var newPoints = parseInt(price) + points;
      this.updatePointsUser(newPoints);
      this.getEarningsOrganizer(price);
    })
  }

  updatePointsUser(newPoints){
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid)
    ref.update({
      points: newPoints,
    })
    .then(() => {
      console.log("HECHO:....")
    })
  }

  getEarningsOrganizer(earning){
    var oldEarnings = 0;
    var ref = firebase.database().ref('/organizers/'+this.keyOrganizerGlobal+'/earnings');
    ref.once('value', (snapshot) => {
      oldEarnings = snapshot.val()
    })
    .then(() => {
      this.updateEarningsOrganizer(earning, oldEarnings);
    })
  }

  updateEarningsOrganizer(earning, oldEarnings) {
    var newEarnings = oldEarnings + parseInt(earning);
    var ref = firebase.database().ref('/organizers/'+this.keyOrganizerGlobal)
    ref.update({
      earnings: newEarnings,
    })
  }

}
