import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-place-details',
  templateUrl: 'place-details.html',
})
export class PlaceDetailsPage {

  place: any;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams) {
    this.place = navParams.data;
  }

  goBack() {this.navCtrl.pop();}

  openMap() {window.open('geo:0,0?q='+this.place.location.latitude+','+this.place.location.longitude+'('+this.place.title+')','_system');}
}