import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

  notifications: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,public toastCtrl: ToastController) {}

  async ionViewWillEnter(){
    await this.getNotifications();
  }

  async getNotifications() {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/notifications');
    await ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        this.notifications.push(child.val())
      })
    })
  }

  delete(notification){
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/notifications');
    ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        if(this.checkNotificationToDelete(child.val(), notification)){
            var deleteRef = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/notifications/'+child.key);
            deleteRef.remove()
            .then(() => {
              var index = this.notifications.indexOf(notification);
              this.notifications.splice(index, 1);
              this.toastCtrl.create({
                message: 'Mensaje eliminado',
                duration: 3000,
              }).present();
            })
          }
      })
    })
  }

  checkNotificationToDelete(child, notification){
    return child.subject == notification.subject && child.messageReply == notification.messageReply && child.subject == notification.subject
  }
}
