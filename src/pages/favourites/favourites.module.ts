import { ProfileComponentModule } from './../../components/profile/profile.component.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavouritesPage } from './favourites';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FavouritesPage,
  ],
  imports: [
    ProfileComponentModule,
    IonicPageModule.forChild(FavouritesPage),
    TranslateModule,
  ],
})
export class FavouritesPageModule {}
