import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase/app';
import * as  moment  from 'moment';

@IonicPage()
@Component({
  selector: 'page-favourites',
  templateUrl: 'favourites.html',
})
export class FavouritesPage {

  favouritesEvents: any [] = [];

  constructor(public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  async ionViewWillEnter() {
    this.favouritesEvents = [];
    await this.getFavouritesEvents();
    await this.checkEvents();
  }

  getFavouritesEvents() {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/favourites');
    ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        this.favouritesEvents.push(child.val())
      })
    })
  }

  async checkEvents() {
    await this.favouritesEvents.forEach((e) => {
      var yearEvent = parseInt(e.date.substring(0, 4));
      var monthEvent = parseInt(e.date.substring(5, 7));
      var dayEvent = parseInt(e.date.substring(8, 10));
      
      var dateEvent = new Date(Date.UTC(yearEvent, monthEvent, dayEvent, 0, 0, 0));
      var timeEvent = dateEvent.getTime()/1000;
      var timeNow = moment().unix();
      var difference = (timeEvent - timeNow);

      if(difference < 0) {
        var index = this.favouritesEvents.indexOf(e);
        this.favouritesEvents.splice(index, 1);
      }
    })
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.ionViewWillEnter()
    .then(() => {
      refresher.complete();
    });
  }

  deleteEvent(event) {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/favourites')
    ref.orderByChild('timestamp').equalTo(event.timestamp)
    .once('value', (snapshot) => {
      snapshot.forEach(function (childSnapshot) {
        ref.child(childSnapshot.key).remove();
      })
    }).then(() => {
      var index = this.favouritesEvents.indexOf(event);
      this.favouritesEvents.splice(index, 1);
    })
  }
}
