import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { EventDetailsPage } from './../event-details/event-details';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { App } from 'ionic-angular';
import firebase from 'firebase/app';
import * as  moment  from 'moment';
import { NotificationsPage } from '../notifications/notifications';

@IonicPage()
@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html',
})
export class FeedPage {

  events: any = [];
  organizers: any = [];
  organizersDetails = [];
  loading: boolean = true;
  nothing: boolean = false;
  notificationsQuantity:number = 0;

  constructor(public navCtrl: NavController, private app: App, public navParams: NavParams, 
    private afAuth: AngularFireAuth, private socialSharing: SocialSharing,
    private toastCtrl:ToastController) {
  }

  async ionViewWillEnter() {
    this.initializeVariables();
    await this.getNotificationsNumber();
    await this.getOrganizers();
    this.loading = false;
    
  }

  initializeVariables() {
    this.loading = true;
    this.nothing = false;
    this.organizers = [];
    this.organizersDetails = [];
    this.events = [];
    this.notificationsQuantity = 0;
  }

  async logout() {
    const result = this.afAuth.auth.signOut();
    if (result) {
      this.app.getRootNavs()[0].setRoot('LoginPage');
    }
  }

  async getNotificationsNumber() {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/notifications');
    await ref.once('value', (snapshot) => {
      snapshot.forEach(() => {
        this.notificationsQuantity++;
      })
    })
  }

  async getOrganizers() {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/feed');
    await ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        this.organizers.push(child.val());
      })
    })
    await this.getImageAndNameOrganizer();
    await this.getEvents();

    this.events = this.events.reverse();
    if (this.organizers.length == 0) this.nothing = true;
  }

  async getEvents() {
    this.organizers.forEach((element) => {
      var ref = firebase.database().ref('/organizers/'+element+'/events').orderByChild('timestamp');
      ref.once('value', (snapshot) => {
        snapshot.forEach((child) => {
          this.events.push(child.val())
          this.setImageOrganizer(child.val());
        })
      })
    })
  }

  async getImageAndNameOrganizer() {
    var ref = firebase.database().ref('/organizers/')
    await ref.once('value', (snapshot) => {
        snapshot.forEach((child) => {
          var data = {
            name: child.val().name,
            image: child.val().image_profile,
          }
          this.organizersDetails.push(data)
        })
      })
  }

  setImageOrganizer(event) {
    this.events = this.events.map((el) => {
      if(JSON.stringify(el) === JSON.stringify(event)){
        var o = Object.assign({}, el);
        var url = this.getImage(event);
        o.image_organizer = url;
        return o;
      }else{
        return el;
      }
    })
  }

  getImage(event) {
    for(var i = 0; i < this.organizersDetails.length; i++){
      if(this.organizersDetails[i].name == event.name_organizer) return this.organizersDetails[i].image
    }
  }

  eventDetails(event){this.navCtrl.push(EventDetailsPage, event);}

  openNotificationsPage(){this.navCtrl.push(NotificationsPage); }

  time(timestamp){
    var timeEvent = timestamp;
    var timeNow = moment().unix();
    var difference = (timeNow - timeEvent);
    return Math.floor(difference/60/60/24);
  }

  shareViaWhatsapp(event){
    const string = "Acompañame a este evento\n" + event.title + "\n" 
    + "Lugar: " + event.place + "\n" + "Horario: " + event.timeStart + " - " + event.timeEnd; 
    this.socialSharing.shareViaWhatsApp(string, null, null)
  }

  shareViaTwitter(event){
    const string = "Acompañame a este evento\n" + event.title + "\n" 
    + "Lugar: " + event.place + "\n" + "Horario: " + event.timeStart + " - " + event.timeEnd; 
    this.socialSharing.shareViaTwitter(string, null, null)
  }

  shareViaInstagram(event){
    const string = "Acompañame a este evento\n" + event.title + "\n" 
    + "Lugar: " + event.place + "\n" + "Horario: " + event.timeStart + " - " + event.timeEnd; 
    this.socialSharing.shareViaInstagram(string, null)
  }

  saveToFavourite(event) {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid+'/favourites');
    ref.push({
      UID_organizer: event.UID_org,
      timestamp: event.timestamp,
      name: event.title,
      date: event.date,
    })
    .then(() => {
      this.toastCtrl.create({
        message: 'Guardado en favoritos',
        duration: 3000,
      }).present();
    })
  }

  doRefresh(refresher) {
    this.ionViewWillEnter()
    .then(() => { refresher.complete();});
  }

}
