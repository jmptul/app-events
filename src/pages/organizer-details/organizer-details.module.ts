import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrganizerDetailsPage } from './organizer-details';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    OrganizerDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(OrganizerDetailsPage),
    TranslateModule,
  ],
})
export class OrganizerDetailsPageModule {}
