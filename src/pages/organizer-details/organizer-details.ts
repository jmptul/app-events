import { PlaceDetailsPage } from './../place-details/place-details';
import { EventDetailsPage } from './../event-details/event-details';
import { SendMessagePage } from './../send-message/send-message';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TicketsPage } from '../tickets/tickets';
import { FavouritesPage } from '../favourites/favourites';
import { ProfileDetailsPage } from '../profile-details/profile-details';

import firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-organizer-details',
  templateUrl: 'organizer-details.html',
})
export class OrganizerDetailsPage {

  tabProfileDetails: any;
  tabFavourites: any;
  tabTickets: any;
  organizer: any = [];
  reputation: number = 0;
  quantityOfReputation: number = 0;
  events: any = [];
  places: any = [];

  displayRank:boolean = false;
  displayRankBlank:boolean = true;
  stars_1:boolean = false;
  stars_2:boolean = false;
  stars_3:boolean = false;
  stars_4:boolean = false;
  stars_5:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.organizer = navParams.data;
    this.tabProfileDetails = ProfileDetailsPage;
    this.tabFavourites = FavouritesPage;
    this.tabTickets = TicketsPage;
  }

  async ionViewWillEnter(){
    this.events = [];
    this.places = [];
    await this.getEvents();
    await this.getPlaces();
    await this.getReputation();
    await this.getUserPuntuation();
  }

  goBack() {
    this.navCtrl.pop();
  }

  sendMessage(){
    this.navCtrl.push(SendMessagePage, this.organizer);
  }

  setStars(value){
    switch(value) {
      case 1:
        this.stars_1 = true;
        this.stars_2 = false;
        this.stars_3 = false;
        this.stars_4 = false;
        this.stars_5 = false;
        this.displayRankBlank = false;
        break;
      
      case 2:
        this.stars_1 = false;
        this.stars_2 = true;
        this.stars_3 = false;
        this.stars_4 = false;
        this.stars_5 = false;
        this.displayRankBlank = false;
        break;

      case 3:
        this.stars_1 = false;
        this.stars_2 = false;
        this.stars_3 = true;
        this.stars_4 = false;
        this.stars_5 = false;
        this.displayRankBlank = false;
        break;
      
      case 4:
        this.stars_1 = false;
        this.stars_2 = false;
        this.stars_3 = false;
        this.stars_4 = true;
        this.stars_5 = false;
        this.displayRankBlank = false;
        break;

      case 5:
        this.stars_1 = false;
        this.stars_2 = false;
        this.stars_3 = false;
        this.stars_4 = false;
        this.stars_5 = true;
        this.displayRankBlank = false;
        break;
    }
  }

  async getUserPuntuation() {
    var reputationUser: number = 0;
    var ref = firebase.database().ref('/organizers/'+this.organizer.UID+'/reputation');
    await ref.once('value', (snapshot) => {
      if(snapshot.hasChild(firebase.auth().currentUser.uid)){
        snapshot.forEach((child) => {
          reputationUser = child.val().puntuation;
        })
      }
    }).then(() => {
      this.setStars(reputationUser);
    })
  }

  async getReputation() {
    var reputationSum: number = 0;
    var ref = firebase.database().ref('/organizers/'+this.organizer.UID+'/reputation');
    await ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        this.quantityOfReputation++;
        reputationSum = reputationSum + child.val().puntuation;
      })
    }).then(() => {
      if(reputationSum != 0){
        this.reputation = reputationSum / this.quantityOfReputation;
      }
    })
  }

  async getEvents(){
    var ref = firebase.database().ref('/organizers/'+this.organizer.UID+'/events');
    await ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        this.events.push(child.val())
      })
    })
  }

  async getPlaces(){
    var ref = firebase.database().ref('/organizers/'+this.organizer.UID+'/places');
    await ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
        this.places.push(child.val())
      })
    })
  }

  placeDetails(place){
    this.navCtrl.push(PlaceDetailsPage, place);
  }

  eventDetails(event){
    this.navCtrl.push(EventDetailsPage, event);
  }

  sendRank() {
    this.displayRank ? this.displayRank = false : this.displayRank = true;
  }

  setRank(value) {
    var refCheck = firebase.database().ref('/organizers/'+this.organizer.UID+'/reputation');
    refCheck.once('value', (snapshot) => {
      if(snapshot.hasChild(firebase.auth().currentUser.uid)) {
        var ref = firebase.database().ref('/organizers/'+this.organizer.UID+'/reputation').child(firebase.auth().currentUser.uid);
        ref.update({
          UID: firebase.auth().currentUser.uid,
          puntuation: value,
        }).then(() => {
          this.reputation = 0;
          this.quantityOfReputation = 0;
          this.getReputation();
        })
      }else{
        var ref = firebase.database().ref('/organizers/'+this.organizer.UID+'/reputation').child(firebase.auth().currentUser.uid);
        ref.set({
          UID: firebase.auth().currentUser.uid,
          puntuation: value,
        }).then(() => {
          this.reputation = 0;
          this.quantityOfReputation = 0;
          this.getReputation();
        })
      }
    })

  }

  rankOrganizer(value) {
    switch(value) {
      case 1:
        this.setRank(1);
        this.stars_1 = true;
        this.stars_2 = false;
        this.stars_3 = false;
        this.stars_4 = false;
        this.stars_5 = false;
        this.displayRankBlank = false;
        break;
      
      case 2:
        this.setRank(2);
        this.stars_1 = false;
        this.stars_2 = true;
        this.stars_3 = false;
        this.stars_4 = false;
        this.stars_5 = false;
        this.displayRankBlank = false;
        break;

      case 3:
        this.setRank(3);
        this.stars_1 = false;
        this.stars_2 = false;
        this.stars_3 = true;
        this.stars_4 = false;
        this.stars_5 = false;
        this.displayRankBlank = false;
        break;
      
      case 4:
        this.setRank(4);
        this.stars_1 = false;
        this.stars_2 = false;
        this.stars_3 = false;
        this.stars_4 = true;
        this.stars_5 = false;
        this.displayRankBlank = false;
        break;

      case 5:
        this.setRank(5);
        this.stars_1 = false;
        this.stars_2 = false;
        this.stars_3 = false;
        this.stars_4 = false;
        this.stars_5 = true;
        this.displayRankBlank = false;
        break;
    }
  }
}
