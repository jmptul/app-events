import { PlaceDetailsPage } from './../place-details/place-details';
import { Component, ChangeDetectionStrategy, ChangeDetectorRef,  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase/app';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@IonicPage()
@Component({
  selector: 'page-places',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'places.html',
})
export class PlacesPage {
  searchQuery: any = "";
  places: any = [];
  placesList: any = [];
  placesSorted: any = [];
  loading: boolean = true;
  currentLatitude: any;
  currentLongitude: any;
  searching: boolean = false;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private cdRef:ChangeDetectorRef,
    public geolocation: Geolocation) {
    }
    

    async ionViewWillEnter(){
      this.placesList = [];
      this.placesSorted = [];
      this.places = [];
      await this.getPlaces();
      this.loading = false;
    }

    async getPlaces(){
      let placesOnLoad = [];
      var ref = firebase.database().ref('/organizers/');
      await ref.once('value', (snapshot) => {
        snapshot.forEach((child) => {
            child.forEach((otherChild) => {
              if(otherChild.key == 'places'){
                otherChild.forEach((snap) => {
                  placesOnLoad.push(snap.val());
                })
              }
            })
        })
      }).then(() => {
        this.placesList = placesOnLoad;
        this.places = placesOnLoad;
        this.getLocation();
        if (!this.cdRef['destroyed']) {
          this.cdRef.detectChanges();
        }
      })
    }

    getLocation(){
      this.geolocation.getCurrentPosition()
      .then((resp) => {
        this.currentLatitude = resp.coords.latitude;
        this.currentLongitude = resp.coords.longitude;
        this.getPlacesWithLocation(this.currentLatitude, this.currentLongitude);
       }).catch((error) => {
         console.log('Error getting location', error);
       });
    }
  
    getPlacesWithLocation(userLatitude, userLongitude){
      this.places.forEach((place) => {
        var kilometers = this.placeIsNearToUserLocation(place.location.latitude, place.location.longitude, userLatitude, userLongitude);
        if(kilometers < 50){
          this.placesSorted.push(place);
          this.setKilometers(kilometers, place);
        }
      })
      this.placesSorted.sort((a,b) =>{
        var x = a.kilometers; var y = b.kilometers;
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
      })
      if (!this.cdRef['destroyed']) {
        this.cdRef.detectChanges();
      }
    }

    setKilometers(kilometers, place) {
      this.placesSorted = this.placesSorted.map(function(el) {
        if(JSON.stringify(el) === JSON.stringify(place)){
          var o = Object.assign({}, el);
          var arr = kilometers.toString().split(".");
          o.kilometers = arr[0];
          return o;
        }else{
          return el;
        }
      })
    }
    
    placeIsNearToUserLocation(eventLatitude, eventLongitude, userLatitude, userLongitude) {
      var R = 6371;
    
      var x1 = userLatitude - eventLatitude;
      var dLat = x1 * Math.PI / 180;
      var x2 = userLongitude - eventLongitude;
      var dLon = x2 * Math.PI / 180;
      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
              Math.cos(eventLatitude * Math.PI / 180) * Math.cos(userLatitude * Math.PI / 180) *
              Math.sin(dLon/2) * Math.sin(dLon/2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      var d = R * c;

      return d;
    }

    placeDetails(place) {this.navCtrl.push(PlaceDetailsPage, place);}

    initializeItems() {this.placesSorted = this.places;}

    search() {
      this.initializeItems();
      if(this.searchQuery.length < 1){
        this.searching = false;
        return;
      }
      this.searching = true;
      this.placesSorted = this.placesSorted.filter((place) => place.title.toLowerCase().includes(this.searchQuery.toLowerCase()));
    }
}
