import { PlacesPage } from './../places/places';
import { EventsPage } from './../events/events';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OrganizersPage } from '../organizers/organizers';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  tabEvents: any;
  tabOrganizers: any;
  tabPlaces: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tabEvents = EventsPage;
    this.tabOrganizers = OrganizersPage;
    this.tabPlaces = PlacesPage;
  }

}
