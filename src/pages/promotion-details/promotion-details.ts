import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import JsBarcode from 'jsbarcode';

@IonicPage()
@Component({
  selector: 'page-promotion-details',
  templateUrl: 'promotion-details.html',
})
export class PromotionDetailsPage {

  promotion: any = [];
  datePromotion: any;
  getPromotionValue: boolean = false;
  public picture;
  @ViewChild('barcode') barcode: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.promotion = navParams.data;
    this.datePromotion = this.getDate();
  }

  ionViewDidLoad(){
    this.getPromotionValue = false;
    JsBarcode(this.barcode.nativeElement, this.promotion.titlePromotion);
  }

  goBack() {
    this.navCtrl.pop();
  } 

  getDate() {
    return this.promotion.datePromotion.substring(8, 10) + 
    "/" + this.promotion.datePromotion.substring(5, 7) + 
    "/" + this.promotion.datePromotion.substring(0, 4);
  }

  getPromotion() {
    this.getPromotionValue = true;
  }
}
