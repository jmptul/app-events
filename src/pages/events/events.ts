import { User } from './../../app/models/user.model';
import { EventDetailsPage } from './../event-details/event-details';
import { Component, ChangeDetectorRef, ChangeDetectionStrategy  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase/app';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@IonicPage()
@Component({
  selector: 'page-events',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'events.html',
})
export class EventsPage {

  eventDetailsPage: EventDetailsPage;
  events: any = [];
  eventsList: any = [];
  currentLatitude: any;
  currentLongitude: any;
  loading: boolean = true;
  searching: boolean = false;
  user = {} as User;
  searchQuery: any = "";


  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private cdRef:ChangeDetectorRef,
    public geolocation: Geolocation) {
      this.user.location = false;
    }
    
  async ionViewWillEnter(){
    this.events = [];
    this.getEvents();
    this.loading = false;
  }

  async getEvents(){
    let eventsOnLoad = [];
    var ref = firebase.database().ref('/organizers/');
    await ref.once('value', (snapshot) => {
      snapshot.forEach((child) => {
          child.forEach((otherChild) => {
            if(otherChild.key == 'events'){
              otherChild.forEach((snap) => {
                eventsOnLoad.push(snap.val());
                
              })
            }
          })
      })
    }).then(() => {
      this.eventsList = eventsOnLoad;
      this.events = eventsOnLoad;
      if(this.user.location){
        this.getLocation();
      }
      if (!this.cdRef['destroyed']) {
        this.cdRef.detectChanges();
      }
    })
  }

  initializeItems() {
    this.events = this.eventsList;
  }

  desactivateLocation(){
    this.user.location = false;
    this.ionViewWillEnter();
  }

  getLocation(){
    this.geolocation.getCurrentPosition()
    .then((resp) => {
      this.user.location = true;
      this.currentLatitude = resp.coords.latitude;
      this.currentLongitude = resp.coords.longitude;
      this.getEventsWithLocation(this.currentLatitude, this.currentLongitude);
     }).catch((error) => {
       console.log('Error getting location', error);
     });

  }

  getEventsWithLocation(userLatitude, userLongitude){
    this.events.forEach((e) => {
      if(!(this.eventIsNearToUserLocation(e.latitude, e.longitude, userLatitude, userLongitude) < 100)){
        var index = this.events.indexOf(e);
        this.events.splice(index, 1);
      }
    })
    if (!this.cdRef['destroyed']) {
      this.cdRef.detectChanges();
    }
  }
  
  eventIsNearToUserLocation(eventLatitude, eventLongitude, userLatitude, userLongitude) {
    var R = 6371;
    
    var x1 = userLatitude - eventLatitude;
    var dLat = x1 * Math.PI / 180;
    var x2 = userLongitude - eventLongitude;
    var dLon = x2 * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(eventLatitude * Math.PI / 180) * Math.cos(userLatitude * Math.PI / 180) *
            Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;

    return d;
  }

  eventDetails(event){
    this.navCtrl.push(EventDetailsPage, event);
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.ionViewWillEnter()
    .then(() => {
      refresher.complete();
    });
  }

  search() {
    console.log("Entro")
    if(this.searchQuery.length > 0){
      this.searching = true;
    }else{
      this.searching = false;
    }
    this.initializeItems();

    if(this.searchQuery.length < 1){
      return;
    }

    this.events = this.events.filter((event) => event.title.toLowerCase().includes(this.searchQuery.toLowerCase()));
  }
}
