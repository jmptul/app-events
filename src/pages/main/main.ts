import { ProfilePage } from './../account/account';
import { SearchPage } from './../search/search';
import { FeedPage } from './../feed/feed';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {

  tabFeed: any;
  tabSearch: any;
  tabProfile: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.tabFeed = FeedPage;
      this.tabSearch = SearchPage;
      this.tabProfile = ProfilePage;
  }
}
