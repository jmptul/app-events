import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-send-message',
  templateUrl: 'send-message.html',
})
export class SendMessagePage {
  organizer: any;
  subject: string;
  message: string;
  number_messages: number;
  sending: boolean = false;
  success: boolean = false;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public toastCtrl: ToastController) {
    this.organizer = navParams.data;
  }

  send(){
    this.sending = true;
    var ref = firebase.database().ref('/organizers/'+this.organizer.UID+'/messages');
    ref.push({
      subject: this.subject,
      message: this.message,
      user: firebase.auth().currentUser.uid,
    }).then(() => {
      this.updateCounter();
      this.sending = false;
      const toast = this.toastCtrl.create({
        message: 'Mensaje enviado',
        duration: 3000
      });
      toast.present();
    })
  }

  updateCounter(){
    var db = firebase.database();
    var ref = db.ref('/organizers/'+this.organizer.UID+'/number_messages');
    ref.once('value', (snapshot) => {
      this.number_messages = snapshot.val()
      this.number_messages++
      this.update();
    })
  }

  update(){
    var db = firebase.database();
    var ref = db.ref().child('/organizers/'+this.organizer.UID);
    ref.update({
      number_messages: this.number_messages,
    })
  }

}
