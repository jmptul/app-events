import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SendMessagePage } from './send-message';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SendMessagePage,
  ],
  imports: [
    IonicPageModule.forChild(SendMessagePage),
    TranslateModule,
  ],
})
export class SendMessagePageModule {}
