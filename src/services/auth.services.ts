import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase/app';

@Injectable()
export class AuthService {
  private user: firebase.User;

  constructor(public afAuth: AngularFireAuth){
    afAuth.authState.subscribe(user => {
      this.user = user;
    })
  }

  signInWithEmail(credetendials){
    return this.afAuth.auth.signInWithEmailAndPassword(credetendials.email,
      credetendials.password);
  }

  get authenticated(): boolean {
    return this.user !== null;
  }

  logout(): Promise<void> {
    return this.afAuth.auth.signOut();
  }
}