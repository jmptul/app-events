import { PointsPage } from './../../pages/points/points';
import { NavController } from 'ionic-angular';
import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'profile',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'profile.html'
})
export class ProfileComponent {

  name:string;
  followingQuantity:number;
  points:number;
  image:string;

  constructor(private cdRef:ChangeDetectorRef, private navCtrl: NavController) {
    this.getDataUser();
  }

  ionViewWillEnter(){
    this.getDataUser();
  }

  getDataUser() {
    var ref = firebase.database().ref('/users/'+firebase.auth().currentUser.uid)
    ref.once('value', (snapshot) => {
      this.name = snapshot.val().name
      this.followingQuantity = snapshot.val().followQuantity
      this.points = snapshot.val().points
      this.image = snapshot.val().image
    }).then(() => {
      if (!this.cdRef['destroyed']) {
        this.cdRef.detectChanges();
      }
    })
  }

  openPointsPage() {
    this.navCtrl.push(PointsPage);
  }

}
