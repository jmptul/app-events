import { ProfileComponent } from './profile';
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "ionic-angular";

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
  ],
  declarations: [ProfileComponent],
  exports: [ProfileComponent]
})
export class ProfileComponentModule {}

