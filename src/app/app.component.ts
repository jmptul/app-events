import { Globalization } from '@ionic-native/globalization/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { TranslateService } from '@ngx-translate/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = 'LoginPage';

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    translateService: TranslateService,
    screenOrientation: ScreenOrientation,
    globalization: Globalization) {
    platform.ready().then(() => {
    
      screenOrientation.lock(screenOrientation.ORIENTATIONS.PORTRAIT);
      translateService.setDefaultLang('es');
      statusBar.styleDefault();
      splashScreen.hide();
      globalization.getPreferredLanguage()
      .then((res) => {
        if(res.value == "es-ES") {
          translateService.use('es');
        }else if(res.value == "en-EN"){
          translateService.use('en');
        }
      })
      .catch(e => console.log(e))
      
    });
  }


  
}
