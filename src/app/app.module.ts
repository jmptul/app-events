import { Globalization } from '@ionic-native/globalization/ngx';
import { PaymentDataPageModule } from './../pages/payment-data/payment-data.module';
import { SendMessagePageModule } from './../pages/send-message/send-message.module';
import { OrganizerDetailsPageModule } from './../pages/organizer-details/organizer-details.module';
import { ProfileComponentModule } from './../components/profile/profile.component.module';
import { FavouritesPageModule } from './../pages/favourites/favourites.module';
import { ProfileDetailsPageModule } from './../pages/profile-details/profile-details.module';
import { ProfilePageModule } from './../pages/account/account.module';
import { SearchPageModule } from './../pages/search/search.module';
import { FeedPageModule } from './../pages/feed/feed.module';
import { FIREBASE_CONFIG } from './app.firebase.config';
import { MainPageModule } from './../pages/main/main.module';
import { MainPage } from './../pages/main/main';
import { AuthService } from './../services/auth.services';
import { LoginPageModule } from './../pages/login/login.module';
import { LoginPage } from '../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';


import { MyApp } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { EventsPageModule } from '../pages/events/events.module';
import { OrganizersPageModule } from '../pages/organizers/organizers.module';
import { TicketsPageModule } from '../pages/tickets/tickets.module';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { EventDetailsPageModule } from '../pages/event-details/event-details.module';
import { EventDetailsPage } from '../pages/event-details/event-details';
import { PlaceDetailsPageModule } from '../pages/place-details/place-details.module';
import { NotificationsPageModule } from '../pages/notifications/notifications.module';
import { NotificationsPage } from '../pages/notifications/notifications';
import { TicketDetailsPageModule } from '../pages/ticket-details/ticket-details.module';
import { PlacesPageModule } from '../pages/places/places.module';
import { PointsPageModule } from '../pages/points/points.module';
import { PromotionDetailsPageModule } from '../pages/promotion-details/promotion-details.module';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    LoginPageModule,
    MainPageModule,
    FeedPageModule,
    SearchPageModule,
    ProfilePageModule,
    EventsPageModule,
    OrganizersPageModule,
    ProfileDetailsPageModule,
    FavouritesPageModule,
    ProfileComponentModule,
    TicketsPageModule,
    EventDetailsPageModule,
    OrganizerDetailsPageModule,
    SendMessagePageModule,
    PlaceDetailsPageModule,
    NotificationsPageModule,
    PaymentDataPageModule,
    TicketDetailsPageModule,
    PlacesPageModule,
    PointsPageModule,
    PromotionDetailsPageModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp,{
      scrollPadding: false,
      scrollAssist: false
    }),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    HttpClientModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    MainPage,
    EventDetailsPage,
    NotificationsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthService,
    Geolocation,
    Globalization,
    NativeGeocoder,
    ScreenOrientation,
    NativePageTransitions,
    SocialSharing,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
