export interface User{
  email: string,
  password: string,
  name: string,
  lastname: string,
  points: 0,
  followingQuantity: 0,
  messagesQuantity: 0,
  location: boolean,
  buy: boolean,
  tickets: boolean,
  image: any,
}